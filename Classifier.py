
"""
Name:        Classifier.py
Authors:     Joao P. Ramos - Written at University of Coimbra, Coimbra,
                Portugal
Contact:     jpramos@dei.uc.pt
Created:     February 2017
Description: This class defines a classifier, the individual of the population.

"""

# Import required modules --
from Constants import *
from RealRep import RealRep
from DiscrRep import DiscreteRep
from Parameters import Parameters

import copy as cp
import random as rand
import math
import logging
# --------------------------

# Get Logger info
LOGGER = logging.getLogger(__name__)


class Classifier:

    def __init__(self):
        """ Class Constructor

        """

        # classifier's action
        self.action = None

        # classifier's condition
        self.condition = []

        # condition's specified indexes
        self.spec_index = []

        # classifier's parameters
        self.cl_param = Parameters()

        # what was seen
        self.was_seen = []

        # what others predict
        self.others_prediction = []

    # end Class Constructor


    def covering(self, tstamp, instance, action):
        """ covering mechanism

        """

        # build condition based on the current instance
        for (i, att) in enumerate(cons.env.attribute_info[:-1]):
            if rand.random() < cons.p_WD:
                if att['is_continuous']:
                    tmp = RealRep(i)
                    tmp.build_interval(instance[i])
                    self.condition.append(tmp)
                else:
                    self.condition.append(DiscreteRep(i, instance[i]))
                self.spec_index.append(i)


        # action is the same as the current instance
        self.action = float(action)

        # init a new set of parameters with pre-defined values
        self.cl_param = Parameters(tstamp, 1)

        # init was_seen and others_prediction
        self.was_seen = [0] * cons.env.num_classes
        self.others_prediction = [0] * cons.env.num_classes

    # end covering


    def from_str(self, condition, action):
        """ create a classifier from string

        """

        for (i, att) in enumerate(cons.env.attribute_info[:-1]):
            if condition[i] != '#':
                if att['is_continuous']:
                    tmp = RealRep(i)
                    tmp.build_interval(float(condition[i]))
                    self.condition.append(tmp)
                else:
                    self.condition.append(DiscreteRep(i, float(condition[i])))
                self.spec_index.append(i)


        # copy action
        self.action = float(action)

        # init a new set of parameters with pre-defined values
        self.cl_param = Parameters(1, 1)

        # init was_seen and others_prediction
        self.was_seen = [0] * cons.env.num_classes
        self.others_prediction = [0] * cons.env.num_classes

    # end from_str


    def specify(self, instance):
        """ Specify operator for a classifier

            Specifies do not care symbols, with a probability P_SP, in a
            classifier's condition

        """

        has_changed = False
        for ind in xrange(cons.env.num_attributes):

            if ind not in self.spec_index:
                if rand.random() < cons.P_sp:
                    has_changed = True
                    att_info = cons.env.attribute_info[ind]

                    if att_info['is_continuous']:
                        tmp = RealRep(ind)
                        tmp.build_interval(instance[ind])
                        self.condition.append(tmp)
                    else:
                        self.condition.append(DiscreteRep(ind,
                            instance[ind]))
                    self.spec_index.append(ind)

        if has_changed:
            self.spec_index, self.condition = (list(x) for x in zip(*sorted(zip(self.spec_index, self.condition), key=lambda x: x[0])))

    # end specify



    def clone(self, other_cl):
        """ Clone other classifier

        """

        self.action = other_cl.action

        self.condition = cp.deepcopy(other_cl.condition)
        self.spec_index = cp.deepcopy(other_cl.spec_index)

        self.was_seen = [0] * cons.env.num_classes
        self.others_prediction = [0] * cons.env.num_classes

        #self.was_seen = cp.deepcopy(other_cl.was_seen)
        #self.others_prediction = cp.deepcopy(other_cl.others_prediction)

    # end clone


    def get_prediction(self):
        """ get classifier prediction

        """

        return self.cl_param.prediction
    # end get_prediction


    def get_eps(self):
        """ get classifier prediction error

        """

        return self.cl_param.eps
    # end get_eps

    def get_acc(self):
        """ Get classifier accuracy
        
        """

        return self.cl_param.acc
    # end get_acc


    def get_exp(self):
        """ Get classifier experience

        """

        return self.cl_param.exp
    # end get_exp


    def get_numerosity(self):
        """ Get classifier numerosity

        """

        return self.cl_param.numerosity
    # end get_numerosity


    def get_Mfitness(self):
        """ Get Macro fitness

        """

        return self.cl_param.fitness
    # end get_Ffitness


    def get_mfitness(self):
        """ get micro fitness

        """

        return self.cl_param.get_micro_fitness()
    # end get_mfitness


    def get_time(self):
        """ get classifiers time

        """

        return self.cl_param.ts
    # end get_time


    def set_time(self, ts):
        """ set classifiers time

        """

        self.cl_param.ts = ts
    # end set_time


    def update_parameters(self, P, c_size):
        """ update classifier

        """

        self.cl_param.update_parameters(P, c_size, self.action)

    # end update_parameters


    def update_fitness(self, k, k_sum):
        """ update fitness

        """

        self.cl_param.update_fitness(k, k_sum)
    # end update_fitness


    def update_seen(self, inst_class):
        """ update what was seen in the match set

            Surprisingly popular edition

        """

        ind = cons.env.all_classes.index(inst_class)
        self.was_seen[ind] += 1

    # end update_seen


    def update_others_prediction(self, m_set):
        """ update what others predict in the match set

            Surprisingly popular edition

        """

        if m_set.size() > 1:
            for c in m_set.set:
                ind = cons.env.all_classes.index(c.action)
                self.others_prediction[ind] += 1

            #ind = cons.env.all_classes.index(self.action)
            #self.others_prediction[ind] -= 1

    # end update_others_prediction


    def get_others_prediction(self, class_ind):
        """ get this classifier prediction of others endorsements for a given
        class

        """

        if sum(self.others_prediction) > 0:
            return (self.others_prediction[class_ind] /
                    float(sum(self.others_prediction)))
        else:
            return self.others_prediction[class_ind]
    # end get_others_prediction


    def get_bts_score(self, avg_endors, gmean_pred):
        """ Get Bayesian Truth Serum (BTS) score of this classifier:

            avg_endors is the average of the endorsements
            gmean_pred is the geometric mean of the predictions

            Return BTS score of this individual
        """

        # BTS score is the sum of 'information score' + 'prediction score'

        # information score
        c_ind = cons.env.all_classes.index(self.action)
        info_score = math.log10(avg_endors[c_ind] / gmean_pred[c_ind])

        # prediction score
        sum_pred = float(sum(self.others_prediction))
        if sum_pred > 0:
            o_preds = [x/sum_pred for x in self.others_prediction]
            pred_score = sum([a_endor * math.log10(o_pred / a_endor) for (a_endor,
                o_pred) in zip(avg_endors, o_preds) if a_endor > 0 and (o_pred / a_endor) > 0])
        else:
            pred_score = 0


        bts_score = info_score + pred_score

        return bts_score
    # end get_bts_score


    def incr_numerosity(self, value):
        """ update numerosity

        """

        self.cl_param.numerosity += value
    # end incr_numerosity


    def set_avfit(self, value):
        """ update average fitness on the present set

        """

        self.cl_param.avFit = value
    # end set_avfit

    
    def deletion_vote(self):
        """ Calculate deletion vote for this classifier

            - avFit is the average fitness of the set

            Returns the estimated vote
        """

        vote = self.cl_param.cs_size * self.cl_param.numerosity

        if self.cl_param.exp >= cons.theta_Del and self.cl_param.get_micro_fitness() < cons.delta * self.cl_param.avFit:
            vote *= self.cl_param.avFit / self.cl_param.get_micro_fitness()

        return vote
    # end deletion_vote


    def equals(self, other_cl):
        """ Compares the classifier with another classifier other_cl

            Returns True if they are equal; False otherwise
        """

        if self.action != other_cl.action:
            return False

        if len(self.spec_index) != len(other_cl.spec_index):
            return False

        self_ind = sorted(self.spec_index)
        other_ind = sorted(other_cl.spec_index)

        if self_ind == other_ind:
            for ind in xrange(len(self_ind)):
                if self.condition[ind].equals(other_cl.condition[ind]):
                    pass
                else:
                    return False
            return True

        return False
    # end equals


    def match(self, inst):
        """ Check if the classifier matches the current instance

            Returns True if it matches; False otherwise
        """

        for ind in xrange(len(self.spec_index)):

            inst_allele = inst[self.spec_index[ind]]

            if self.condition[ind].match(inst_allele):
                continue
            else:
                return False

        return True
    # end match

    def could_subsume(self):
        """ Indicates if the classifier can subsume

            The classifier subsume if it is sufficiently accurate and
            experienced

            Returns True if it can subsume; False otherwise
        """

        if self.cl_param.exp > cons.theta_Sub and self.cl_param.acc > cons.acc_0:
            return True
        else:
            return False
    # end could_subsume


    def is_more_general(self, other_cl):
        """ Checks if the classifier is more general than the classifier
            other_cl

            - other_cl is the classifier to be compared against

            Returns True if it is more general than other_cl; False otherwise
        """

        if len(self.condition) >= len(other_cl.condition):
            return False

        for ind in xrange(len(self.spec_index)):
            if self.spec_index[ind] not in other_cl.spec_index:
                return False

            o_ref = other_cl.spec_index.index(self.spec_index[ind])

            if not self.condition[ind].is_more_general(other_cl.condition[o_ref]):
                return False

        return True
    # end is_more_general


    def does_subsume(self, other_cl):
        """ Tests whether the classifier can subsume the other classifier
            other_cl

            - other_cl is the potentially subsumed classified

            Returns True if it can subsume; False otherwise

        """

        if self.action == other_cl.action:
            if self.could_subsume():
                if self.is_more_general(other_cl):
                    return True

        return False
    # end does_subsume


    def print_condition(self, print2screen=False):
        """ Print classifier's condition

        """

        str = ''
        for att_ind in xrange(len(cons.env.attribute_info[:-1])):
            if att_ind in self.spec_index:
                this_ind = self.spec_index.index(att_ind)
                str += '{} '.format(self.condition[this_ind].allele)
            else:
                str += '# '

        str = '[' + str + ']'
        if print2screen:
            print(str)

        return str
    #end print_condition
    
    def print_cl(self):
        """ Print Classifier

            Print condition and action of the classifier
        """

        for (att_ind, att) in enumerate(cons.env.attribute_info[:-1]):

            if att_ind in self.spec_index:
                this_ind = self.spec_index.index(att_ind)
                print('{}\t{}'.format(att['att_name'],
                    self.condition[this_ind].allele))
            else:
                print('{}\t#'.format(att['att_name']))

        print('action\t{}'.format(self.action))
    # end print_cl


    def print_atts(self, print_header=True):
        """ Print Classifier attributes

        """

        self.cl_param.print_atts()
    # end print_atts

