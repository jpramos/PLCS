
"""
Name:        RuleManagement.py
Authors:     Joao P. Ramos - Written at University of Coimbra, Coimbra,
                Portugal
Contact:     jpramos@dei.uc.pt
Created:     February 2017
Description: This class manages expert rules activation.

"""

# Import required modules ---------------------
from Constants import *
from Rule import Rule

import sys
import logging
# ---------------------------------------------


# Get Logger info
LOGGER = logging.getLogger(__name__)

class RuleManagement:

    def __init__(self, rule_file, att_list):
        """ Class Constructor

            - rule_file is the name of the file with the loaded rules
            - att_list is the list of attributes in the environment

        """

        self.ruleset = None

        self.load_rules(rule_file, att_list)
        
        pass
    # end Class Constructor


    def load_rules(self, ruleFile, att_list):
        """ Load the rule file. 
        
        """
        print("RuleManagement: Loading rules... {}".format(str(ruleFile)))

        try:
            rule_list = []
            f = open(ruleFile, 'r')

            for line in f:
                r_vals = line.strip('\n').split(' ')
                att = r_vals[1]
                att_ref = att_list.index(att)
                th = float(r_vals[3])
                action = float(r_vals[0])
                conf_level = float(r_vals[4])
                op = r_vals[2]

                rule_list.append(Rule(att_ref, att, op, th, action, conf_level))

        except IOError, (errno, strerror):
            print ("Could not Read File!")
            print ("I/O error(%s): %s" % (errno, strerror))
            raise
        except:
            print ("Unexpected error: ", sys.exc_info()[0])
            raise

        self.ruleset = rule_list

    # end load_rules


    def check_rule_activation(self, att_ref, inst_val, inst_action):
        """ Check for rule activation

            Check in the existing rule pool for a rule that is activated 

        """

        if inst_val == cons.label_missing_data:
            return 0

        c_bias = []
        c_th = []
        for rule in self.ruleset:
            if rule.att_ref == att_ref:# and inst_action == rule.action:
                max_v = cons.env.attribute_info[att_ref]['max_value']
                min_v = cons.env.attribute_info[att_ref]['min_value']
                c_bias.extend([rule.activate_rule(inst_val, max_v, min_v)])
                c_th.extend([rule.threshold])

        bias = sum(c_bias)

        if bias > 0:
            m_bias = max(c_bias)
            m_th = c_th[c_bias.index(m_bias)]
        else:
            m_bias = -1
            m_th = -1

        return bias, m_bias, m_th

        #if not activ_th:
        #    return 0
        #else:
        #    return max(activ_th)
    # end check_rule_activation


    def match_rule_allele(self, cl_att, cl_action):
        """ Find rules that match allele's conditions

            Find rules that are activated with allele's condition

            Returns:
                * bias - the sum of all the rule's contributions
                * m_bias - the maximum bias found
                * m_th - the respective threshold for the maximum bias found
        """

        bias = 0
        c_bias = []
        c_th = []
        for rule in self.ruleset:
            if rule.att_ref == cl_att.allele_index and cl_action == rule.action:
                max_v = cons.env.attribute_info[cl_att.allele_index]['max_value']
                min_v = cons.env.attribute_info[cl_att.allele_index]['min_value']
                if cl_att.__class__.__name__ == 'DiscreteRep':
                    c_bias.extend([rule.activate_rule(cl_att.allele, max_v,
                        min_v)])
                else:
                    if rule.operator == '>':
                        c_bias.extend([rule.activate_rule(cl_att.allele[0],
                            max_v, min_v)])
                    elif rule.operator == '<':
                        c_bias.extend([rule.activate_rule(cl_att.allele[1],
                            max_v, min_v)])
                c_th.extend([rule.threshold])

        bias = sum(c_bias)

        if bias > 0:
            m_bias = max(c_bias)
            m_th = c_th[c_bias.index(m_bias)]
        else:
            m_bias = -1
            m_th = -1

        return bias, m_bias, m_th
    # end match_rule_allele


    def match_rule_classifier(self, cl):
        """ Find rules that match classifier attributes

            Find rules that are activated with classifier's attributes

        """

        bias = 0
        for cl_att in cl.condition:
            (b, m_b, m_th) = self.match_rule_allele(cl_att, cl.action)
            bias += b

        #bias = 0
        #for cl_att in cl.condition:
        #    for rule in self.ruleset:
        #        if rule.att_ref == cl_att.allele_index and cl.action == rule.action:
        #            #print('rule {}/{}/{}-{}'.format(rule.att, rule.action,
        #            #    rule.threshold, cl_att.allele))
        #            max_v = cons.env.attribute_info[cl_att.allele_index]['max_value']
        #            min_v = cons.env.attribute_info[cl_att.allele_index]['min_value']
        #            if cl_att.__class__.__name__ == 'DiscreteRep':
        #                bias += rule.activate_rule(cl_att.allele, max_v, min_v)
        #            else:
        #                if rule.operator == '>':
        #                    bias += rule.activate_rule(cl_att.allele[0], max_v, min_v)
        #                elif rule.operator == '<':
        #                    bias += rule.activate_rule(cl_att.allele[1], max_v, min_v)
        #print('bias: {}'.format(bias))

        #print('----------------')

        return bias
    # end match_rule_classifier
