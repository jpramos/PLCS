
"""
Name:        Statistics.py
Authors:     Joao P. Ramos - Written at University of Coimbra, Coimbra,
                Portugal
Contact:     jpramos@dei.uc.pt
Created:     February 2017
Description: This class calculates statistics to evaluate the population.

"""

# Import required modules ---------------------
from Constants import *

import logging
# ---------------------------------------------


# Get Logger info
LOGGER = logging.getLogger(__name__)

class Statistics:

    def __init__(self, y_true, y_score):
        """ Class Constructor

            y_true - the true action
            y_score - the estimated action

        """

        self.truth = y_true
        self.predicted = y_score

        self.TP = 0
        self.TN = 0
        self.FP = 0
        self.FN = 0

        self.calc_matrix()

    # end Class Constructor


    def calc_matrix(self):
        """ calculate confusion matrix

        """

        for (t, p) in zip(self.truth, self.predicted):
            if t:
                if t == p:
                    self.TP += 1
                else:
                    self.FN += 1
            else:
                if t == p:
                    self.TN += 1
                else:
                    self.FP += 1
    
    # end calc_matrix


    def specificity(self):
        """ Calculate specificity

            specificity = TN / (TN + FP)

            Returns de calculated value
        """

        return self.TN / float(self.TN + self.FP)
    # end specificity


    def sensitivity(self):
        """ Calculate sensitivity

            sensitivity = TP / (TP + FN)

            Returns the calculated value
        """

        return self.TP / float(self.TP + self.FN)
    # end sensitivity


    def g_mean(self):
        """ Calculate the geometric mean

            g_mean = (sensitivity * specificity) ** .5

            Returns the calculated value
        """

        return (self.sensitivity() * self.specificity()) ** .5
    # end g_mean


    def accuracy(self):
        """ Calculate accuracy

            acc = (TP + TN)/ (TP + TN + FP + FN)

            Returns the calculated value
        """

        return (self.TP + self.TN) / float(self.TP + self.TN + self.FN + self.FP)
    # end accuracy

