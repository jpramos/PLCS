
"""
Name:        AccWeightedPrediction.py
Authors:     Joao P. Ramos - Written at University of Coimbra, Coimbra,
                Portugal
Contact:     jpramos@dei.uc.pt
Created:     February 2017
Description: This class builds an accuracy weighted prediction array to select the best action.

"""

# Import required modules ---------------------
from Constants import *
from PredictionArray import PredictionArray
from MatchSet import MatchSet

import logging
# ---------------------------------------------


# Get Logger info
LOGGER = logging.getLogger(__name__)

class AccWeightedPrediction(PredictionArray):

    def __init__(self, m_set):
        """ Class Constructor

            m_set: match set from which the prediction array is built

        """

        PredictionArray.__init__(self)

        for cl in m_set.set:
            ind = cons.env.all_classes.index(cl.action)
            self.pred_array[ind] += cl.get_acc() * cl.get_Mfitness()
            self.num_sum[ind] += cl.get_numerosity()

    # end Class Constructor

