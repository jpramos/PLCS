
"""
Name:        DataParser.py
Authors:     Joao P. Ramos - Written at University of Coimbra, Coimbra,
                Portugal
Contact:     jpramos@dei.uc.pt
Created:     February 2017
Description: Set of utilities to read and parse a dataset.

"""

# Import required modules --
import string
import re
import numpy as np
# --------------------------

def read_names(file_name):
    """Read and return the names of each attribute.
    
    The file contents follow the UCI dataset's format.
    
    Keyword arguments:
    file_name -- file containing the description of each attribute.
    """
    with open(file_name, 'r') as fd:
        lines = [line.strip() for line in fd]


    lines = lines[1:]
    att_info = []
    for line in lines:

        if len(line) == 0:
           continue 

        (att, info) = line.split(':')
        att = att.strip()
        info = info.strip()[:-1]

        if (info != 'continuous'):
            info = info.split(',')

        att_info.append({'att_name':att, 'att_info':info, 'is_continuous': info
            == 'continuous'})


    
    return att_info
    # end read_names


def read_data(file_name, missing_label='?'):
    """Read data entries of the file_name dataset

    The file contents follow the UCI dataset's format.

    Keyword arguments:
    file_name -- file containing the dataset. Each row is composed by the
    attributes' values and each entry's classification.
    """

    values = np.genfromtxt(file_name, delimiter=',',
            missing_values=missing_label)

    return values
    # end read_data


def print_instance(dta_instance, attr_list, print_layout=0):
    """List the attributes and their values of a given data instance.

    Keyword arguments:
    dta_instance -- list containing the attribute's values of this instance
    attr_list -- list containing the names of the attributes
    print_layout -- column = 0 (default) ; row = 1
    """

    if not print_layout:
        for (x, y) in zip(attr_list, dta_instance):
            print('attr: {} -> {}'.format(x, y))
    else:
        for x in attr_list:
            print('{}\t'.format(x)), 
        print('\n'),
        for x in dta_instance:
            print('{}\t'.format(x)), 
        print('\n'),

    pass
    # end print_instance

