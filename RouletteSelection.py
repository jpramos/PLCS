
"""
Name:        RouletteSelection.py
Authors:     Joao P. Ramos - Written at University of Coimbra, Coimbra,
                Portugal
Contact:     jpramos@dei.uc.pt
Created:     February 2017
Description: This class controls a generic roulette selection mechanism.

"""

# Import required modules ---------------------
from Constants import *
from Selection import Selection

import random as rand
import logging
# ---------------------------------------------


# Get Logger info
LOGGER = logging.getLogger(__name__)

class RouletteSelection(Selection):

    def __init__(self, s_function):
        """ Class Constructor

            s_function - value to which the classifiers are sorted
                - fitness
                - deletion_vote

        """

        Selection.__init__(self, s_function)

        # init roulette
        self.roulette = []

        pass
    # end Class Constructor


    def init(self, set):
        """ Init roulette for a specific set

        """
        self.set = set
        self.roulette = [0] * set.size()
        for (pos, cl) in enumerate(set.set):
            if pos == 0:
                self.roulette[pos] = self.call_s_fun(cl, self.s_fun)
            else:
                self.roulette[pos] = self.roulette[pos-1] + self.call_s_fun(cl,
                        self.s_fun)
    # end init


    def select(self):
        """ select an individual from the set

        """

        sel_point = rand.random() * self.roulette[-1]

        ind = 0
        while(sel_point >= self.roulette[ind]):
            ind += 1

        return self.set.set[ind]
    # end select
        

