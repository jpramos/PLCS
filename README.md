PLCS - Python (implementation of) LCS 
 - Reinforcement learning using the "least surprised"/"surprisingly popular" 
    algorithm [1-2]. 
     
[1] - Prelec, D., Seung, H. S., & McCoy, J. (2017).  
A solution to the single-question crowd wisdom problem.  
Nature, 541(7638), 532–535. http://doi.org/10.1038/nature21054 
 
[2] - Prelec, D., Seung, H. S., & McCoy, J. (2014).  
Finding truth even if the crowd is wrong. Working Paper, 1, 1–13.
