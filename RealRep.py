
"""
Name:        RealRep.py
Authors:     Joao P. Ramos - Written at University of Coimbra, Coimbra,
                Portugal
Contact:     jpramos@dei.uc.pt
Created:     February 2017
Description: This class defines a real representation for the attributes. A
                real value is represented by a Lower-Upper bound representation
                [lower_bound, upper_bound]

"""

# Import required modules --
from Constants import *
from Attribute import Attribute

import random as rand
import logging
# --------------------------

# Get Logger info
LOGGER = logging.getLogger(__name__)

# LOWER AND UPPER BOUND INDEXES
L_B = 0
U_B = 1

class RealRep(Attribute):

    def __init_(self, ai=None, state=None, lb=None, up=None):
        """ Class Constructor

        """

        Attribute.__init__(self, ai)

        self.allele = None

        # allele is represented by a [lower_bound, upper_bound] interval
        if state is None:
            self.allele = [lp, up]
        else:
            self.build_interval(state)
        pass
    # end Class Constructor


    def build_interval(self, state):
        """ build an interval-based continuous representation

        """

        att_info = cons.env.attribute_info[self.allele_index]


        # get attribute's range
        att_range = att_info['max_value'] - att_info['min_value']
        range_radius = rand.randint(25, 75)*.01*att_range / 2.0
        low = state - range_radius
        high = state + range_radius
        cont_val = [low, high]

        cont_val.sort()
        self.allele = cont_val
    # end build_interval

    def mutate(self):
        """ mutate allele

        """

        att_range = self.allele[U_B] - self.allele[L_B]
        mutate_range = rand.random() * .5 * att_range

        if rand.random() < .5:
            # mutate lower bound
            if rand.random() < .5:
                # add
                self.allele[L_B] += mutate_range
            else:
                # substract
                self.allele[L_B] -= mutate_range
        else:
            # mutate upper bound
            if rand.random() < .5:
                # add
                self.allele[U_B] += mutate_range
            else:
                # subtract
                self.allele[U_B] -= mutate_range

        self.allele.sort()

    # end mutate


    def equals(self, o_att):
        """ Compares this attribute with o_att

            Returns True if equals; False otherwise
        """

        if self.allele == o_att.allele:
            return True

        return False
    # end equals


    def match(self, inst_att):
        """ Check if this attribute matches with instance's attribute

            Returns True if it matches; False otherwise
        """

        if self.allele[L_B] < inst_att < self.allele[U_B] or inst_att == cons.label_missing_data:
            return True
        else:
            return False
    # end match


    def is_more_general(self, o_att):
        """ Is more general function for continuous attributes

            Check if the interval of this attribute is more general than o_att
            interval

            Returns True if is more general; False otherwise
        """

        if self.allele[L_B] <= o_att.allele[L_B] and self.allele[U_B] >= o_att.allele[U_B]:
            return True

        return False
    # end is_more_general

