
"""
Name:        Selection.py
Authors:     Joao P. Ramos - Written at University of Coimbra, Coimbra,
                Portugal
Contact:     jpramos@dei.uc.pt
Created:     February 2017
Description: This class selects individuals using specific methods.

"""

# Import required modules ---------------------
from Constants import *

import random as rand
import logging
# ---------------------------------------------


# Get Logger info
LOGGER = logging.getLogger(__name__)

class Selection:

    def __init__(self, s_function):
        """ Class Constructor


        """

        # set
        self.set = None

        if s_function == 'fitness':
            self.s_fun = 'get_mfitness'
        elif s_function == 'deletion_vote':
            self.s_fun = 'deletion_vote'
        elif s_function == 'accuracy':
            self.s_fun = 'get_acc'
        elif s_function == 'prediction':
            self.s_fun = 'get_prediction'
       
        pass
    # end Class Constructor

    
    def call_s_fun(self, obj, f_name):
        """ call sort function for classifiers

        """

        return getattr(obj, f_name)()
    # end call_s_fun

