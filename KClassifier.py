
"""
Name:        KClassifier.py
Authors:     Joao P. Ramos - Written at University of Coimbra, Coimbra,
                Portugal
Contact:     jpramos@dei.uc.pt
Created:     February 2017
Description: This class defines a classifier, the individual of the population,
                with the addition of a bias from external knowledge.

"""

# Import required modules --
from Constants import *
from Classifier import Classifier
from RealRep import RealRep
from DiscrRep import DiscreteRep
from Parameters import Parameters

import random as rand

import logging
# --------------------------

# Get Logger info
LOGGER = logging.getLogger(__name__)


class KnowledgeClassifier(Classifier):

    def __init__(self):
        """ Class Constructor

        """

        Classifier.__init__(self)

    # end Class Constructor
    

    def covering(self, tstamp, instance, action):
        """ covering mechanism

        """

        # build condition based on the current instance
        for (i, att) in enumerate(cons.env.attribute_info[:-1]):
            init_p = rand.random()
            (bias, m_bias, m_th) = cons.env.ruleData.check_rule_activation(i, instance[i],
                    action)
            wd_p = init_p * (1 - bias)
            if wd_p < cons.p_WD:
                if bias > 0:
                    if rand.random() < .5:
                        val = instance[i]   # the condition is created according to
                                            # the data
                    else:
                        val = m_th          # the condition is created according to
                                            # the knowledge
                else:
                    val = instance[i]

                if att['is_continuous']:
                    tmp = RealRep(i)
                    tmp.build_interval(val)
                    self.condition.append(tmp)
                else:
                    self.condition.append(DiscreteRep(i,val))
                self.spec_index.append(i)


        # action is the same as the current instance
        self.action = action

        # init a new set of parameters with pre-defined values
        self.cl_param = Parameters(tstamp, 1)

    # end covering


