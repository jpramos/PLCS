
"""
Name:        MatchSet.py
Authors:     Joao P. Ramos - Written at University of Coimbra, Coimbra,
                Portugal
Contact:     jpramos@dei.uc.pt
Created:     February 2017
Description: This is the class that represents the match set in LCS. In a match
                set are the classifiers that match the condition of the current instance.

"""

# Import required modules ---------------------
from Constants import *
from Set import Set
from CorrectSet import CorrectSet
from Classifier import Classifier
from KClassifier import KnowledgeClassifier

import random as rand
import logging
# ---------------------------------------------


# Get Logger info
LOGGER = logging.getLogger(__name__)

class MatchSet(Set):

    def __init__(self, p_pop):
        """ Class Constructor

            - p_pop is the parent population from which this match set is
              created
        """

        Set.__init__(self)
        
        self.parent_pop = p_pop

        pass
    # end Class Constructor


    def form_correct_set(self, action):
        """ Forms a correct set
        
            Forma a correct set with the classifiers that match both condition
            and action

            Returns a correct set
        """

        c_set = CorrectSet(self)

        for cl in self.set:
            if cl.action == action:
                c_set.set.append(cl)

        return c_set
    # end form_correct_set


    def update_set(self, inst_action):
        """ Update classifiers parameters

            Update classifiers parameters after correct set matching

        """

        for cl in self.set:
            # cl.update_parameters(n_correct, inst_action)
            cl.update_seen(inst_action)
            cl.update_others_prediction(self)

        
        #k_sum = 0
        #k_vec = [0] * len(self.set)

        #for (ind, cl) in enumerate(self.set):
        #    if cl.action != inst_action:
        #        k_vec[ind] = 0
        #    else:
        #        k_vec[ind] = cl.get_acc()
        #        if k_vec[ind] < cons.acc_0:
        #            k_vec[ind] = cons.alpha * ( cons.acc_0 /
        #                    k_vec[ind])**(-1.0*cons.nu)
        #        else:
        #            k_vec[ind] = 1

        #    k_sum += k_vec[ind] * cl.get_numerosity()


        #for (i, cl) in enumerate(self.set):
        #    cl.update_fitness(k_sum, k_vec[i])

    # end update_set


    def covering(self, tstamp, curr_inst):
        """ Covering mechanism

            Creates a new classifier that matches curr_inst and curr_action and
            adds it to all sets

        """

        existing_actions = self.get_actions()
        missing_actions = [x for x in cons.env.all_classes if x not in
                existing_actions]
        new_action = rand.choice(missing_actions)

        # creates new classifier
        if cons.cl_cov == 'KCL':
            cl = KnowledgeClassifier()
        else:
            cl = Classifier()
        cl.covering(tstamp, curr_inst, new_action)

        # TODO See if it's necessary a loop like in KEEL-UCS


        if self.parent_pop.micro_sum() + 1 >= cons.N:

            del_cl = self.parent_pop.delete(False)

            pos = self.has_classifier(del_cl)
            if pos is not None:
                self.set.remove(del_cl)



        # add the new classifier to all sets
        self.set.append(cl)
        self.parent_pop.insert(cl)
        #self.m_set.parent_pop.delete()

    # end covering


    def get_actions(self):
        """ get available actions in this match set

        """

        return set([c.action for c in self.set])
    # end get_classes
