#!/bin/bash

# file names
declare -a file_array=("avgexp.txt"
                            "avgfit.txt"
                            "eval.txt"
                            "maxfit.txt"
                            "minfit.txt"
                            "nclass.txt"
                            "pop.txt"
                            "tmpeval.txt")

# remove files
for file in "${file_array[@]}"; do
    rm "$file"
done

