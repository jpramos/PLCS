
"""
Name:        Parameters.py
Authors:     Joao P. Ramos - Written at University of Coimbra, Coimbra,
                Portugal
Contact:     jpramos@dei.uc.pt
Created:     February 2017
Description: This class holds all the classifiers parameters.

"""

# Import required modules ---------------------
from Constants import *

import logging
# ---------------------------------------------


# Get Logger info
LOGGER = logging.getLogger(__name__)

class Parameters:

    def __init__(self, tstamp=None, c_size=1):
        """ Class Constructor

        """

        # accuracy of the classifier
        self.acc = cons.acc_0

        # number of correct predictions
        self.n_corr_pred = 1

        # fitness of the classifier - macro fitness
        self.fitness = cons.f_I

        # experience of the classifier
        self.exp = 1

        # correct set size estimate
        self.cs_size = c_size

        # time of the classifier
        self.ts = tstamp

        # represents the number of microclassifiers this macroclassifier
        # contains
        self.numerosity = 1

        # generality of the classifier
        self.generality = 1

        self.avFit = 0


        # XCS accuracy
        self.prediction = cons.p_I

        # XCS prediction error
        self.eps = cons.eps_I

    # end Class Constructor


    def xover_parameters(self, par1_p, par2_p, ts):
        """ Crossover Parameters

            define a new parameters object with the result of a crossover
            between two parents

        """

        # accuracy is the mean of both parents
        self.acc = (par1_p.acc + par2_p.acc) / 2.0

        # fitness is a pre-defined percentage of the mean of both parents
        self.fitness = cons.f_red * (par1_p.fitness + par2_p.fitness) / 2.0

        # prediction is the mean of both parents
        self.prediction = (par1_p.prediction + par2_p.prediction) / 2.0

        # prediction error is the mean of both parents
        self.eps = (par1_p.eps + par2_p.eps) / 2.0

        self.ts = ts

        self.cs_size = par1_p.cs_size

        self.n_corr_pred = 1
        self.exp = 1
        self.numerosity = 1
        self.generality = par1_p.generality

    # end xover_parameters


    def clone_parameters(self, other_clp, ts):
        """ Clone parameters

            define a new parameters object by cloning from other_cl parameters

        """

        self.acc = other_clp.acc

        self.fitness = cons.f_red * (other_clp.fitness / other_clp.numerosity)

        self.ts = ts

        self.cs_size = other_clp.cs_size

        self.n_corr_pred = 1
        self.exp = 1
        self.numerosity = 1
        self.generality = other_clp.generality


        # prediction and prediction error
        self.prediction = other_clp.prediction
        self.eps = other_clp.eps

    #end clone_parameters


    def update_parameters(self, P, c_size, cl_class):
        """ Update the parameters of a classifier

            - P is the environment reward
            - c_size is the size of the correct set activated
            - cl_class is the action of this classifier

        """

        self.exp += 1

        # update prediction
        if self.exp < (1.0 / cons.beta):
            self.prediction += (P - self.prediction) / float(self.exp)
        else:
            self.prediction += cons.beta * (P - self.prediction)


        # update prediction error
        if self.exp < (1.0 / cons.beta):
            self.eps += (abs(P - self.prediction) - self.eps) / float(self.exp)
        else:
            self.eps += cons.beta * (abs(P - self.prediction) - self.eps)


        # update correct set size estimate
        if self.exp < (1.0 / cons.beta):
            self.cs_size += (c_size - self.cs_size) / float(self.exp)
        else:
            self.cs_size += cons.beta * (c_size - self.cs_size)


    # end update_parameters


    def update_fitness(self, k_sum, k):
        """ update classifier fitness

            - k_sum is the sum of all accuracies
            - k is the accuracy of the classifier

        """

        self.fitness += cons.beta * ((k * self.numerosity) / k_sum - self.fitness)

    # end update_fitness


    def get_micro_fitness(self):
        """ get fitness of micro-classifier

        """

        return self.fitness / self.numerosity * 1.0
    # end get_micro_fitness


    def print_atts(self, print_header=True):
        """ Print Attributes

        """

        attrs = vars(self)
        if print_header:
            header = self.print_header()
        else:
            header = ''

        info = '\t'.join('%s' % item[1] for item in sorted(attrs.items(),
            key=lambda tup: tup[0]))

        print(header + '\n' + info)
    # end print_atts


    def print_header(self):
        """ Print parameters name

        """

        attrs = vars(self)

        str = '\t'.join('%s' % item[0] for item in sorted(attrs.items(),
            key=lambda tup: tup[0]))

        return str
    # end print_header

