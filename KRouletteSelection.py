
"""
Name:        KRouletteSelection.py
Authors:     Joao P. Ramos - Written at University of Coimbra, Coimbra,
                Portugal
Contact:     jpramos@dei.uc.pt
Created:     February 2017
Description: This class controls a generic roulette selection mechanism.

"""

# Import required modules ---------------------
from Constants import *
from RouletteSelection import RouletteSelection

import random as rand
import logging
# ---------------------------------------------


# Get Logger info
LOGGER = logging.getLogger(__name__)

class KnowledgeRouletteSelection(RouletteSelection):

    def __init__(self, s_function):
        """ Class Constructor

            s_function - value to which the classifiers are sorted
                - fitness
                - deletion_vote

        """

        RouletteSelection.__init__(self, s_function)

        pass
    # end Class Constructor

    
    def init(self, set):
        """ Init roulette for a specific set

        """
        self.set = set
        self.roulette = [0] * set.size()
        for (pos, cl) in enumerate(set.set):
            bias = cons.env.ruleData.match_rule_classifier(cl)
            curr_prob = self.call_s_fun(cl, self.s_fun)
            curr_prob = curr_prob * (1 + bias)
            if pos == 0:
                self.roulette[pos] = curr_prob 
            else:
                self.roulette[pos] = self.roulette[pos-1] + curr_prob
    # end init


