
"""
Name:        Constants.py
Authors:     Joao P. Ramos - Written at University of Coimbra, Coimbra,
                Portugal
Contact:     jpramos@dei.uc.pt
Created:     February 2017
Description: Configuration parameters for the LCS.

"""

# Import required modules --


class Constants:
    def __init__(self):
        """ Class Constructor

        """

        pass
    #end Class Constructor

    def set_constants(self, params):
        """ set constants values
        
            Assigns the value for each parameter

        """

        self.namesFile = params['namesFile']
        self.trainFile = params['trainFile']    # Data and expert knowledge
        self.testFile = params['testFile']
        self.ruleFile = params['ruleFile']

        self.max_iter = float(params['max_iter'])

        self.dont_care_sym = params['dont_care_sym']
        self.label_missing_data = params['label_missing_data']

        self.N = int(params['N'])               # Maximum population size
        self.beta = float(params['beta'])       # Learning rate

        self.alpha = float(params['alpha'])     # used in the fitness
        self.eps_0 = float(params['eps_0'])
        self.nu = float(params['nu'])

        self.gamma = float(params['gamma'])     # discount factor

        self.theta_GA = float(params['theta_GA']) # GA threshold
        self.chi = float(params['chi'])         # probability of crossover
        self.mu = float(params['mu'])           # probability of mutation

        self.theta_Del = float(params['theta_Del'])
        self.delta = float(params['delta'])

        self.theta_Sub = float(params['theta_Sub'])

        self.p_WD = float(params['p_WD'])             # probability of a wild
                                                    # card when covering 

        self.p_I = float(params['p_I'])               # init values for new
        self.eps_I = float(params['eps_I'])           # classifiers
        self.acc_0 = float(params['acc_0'])
        self.f_I = float(params['F_I'])

        self.p_explr = float(params['p_explr'])     # probability during action
                                                    # selection of choosing the action uniform randomly
        
        self.theta_mna = int(params['theta_mna'])

        self.P_sp = float(params['P_sp'])
        self.N_sp = int(params['N_sp'])

        self.do_GA_subsumption = params['do_GA_subsumption'] == 'True'
        self.do_actionset_subsumption = params['do_actionset_subsumption'] == 'True'

        self.crossover = params['crossover']

        self.mutation = params['mutation']

        self.ga_sel = params['ga_selection']
        self.ga_tour_size = float(params['ga_tour_size'])

        self.del_sel = params['del_selection']
        self.del_tour_size = float(params['del_tour_size'])

        self.cl_cov = params['cl_cov']

        self.f_red = float(params['f_red'])

        self.env = None

    #end set_constants

        


cons = Constants()
