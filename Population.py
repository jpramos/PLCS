
"""
Name:        Population.py
Authors:     Joao P. Ramos - Written at University of Coimbra, Coimbra,
                Portugal
Contact:     jpramos@dei.uc.pt
Created:     February 2017
Description: This is the class tha represents the population in a LCS.

"""

# Import required modules ---------------------
from Constants import *
from Set import Set
from Classifier import Classifier
from MatchSet import MatchSet
from TournamentSelection import TournamentSelection
from RouletteSelection import RouletteSelection
from AccWeightedPrediction import AccWeightedPrediction
from PredWeightedPrediction import PredWeightedPrediction
from BTSPrediction import BTSPrediction

import logging
# ---------------------------------------------


# Get Logger info
LOGGER = logging.getLogger(__name__)

class Population(Set):

    def __init__(self, init_pop=1, fromfile_name=None):
        """ Class Constructor

            Inits population set. It can be initialized:
                (1) as an empty population
                (2) at random
                (3) from previous set of knowledge rules
        """

        Set.__init__(self)

        if init_pop == 1:
            self.empty_population()
        elif init_pop == 2:
            self.random_population()
        elif init_pop == 3:
            self.expert_population()
        elif init_pop == 4:
            self.fromfile_population(fromfile_name)


        if cons.del_sel == 'RWS':
            self.selection = RouletteSelection('deletion_vote')
        else:
            self.selection = TournamentSelection(cons.del_tour_size, 'deletion_vote')

        pass
    # end Class Constructor


    def empty_population(self):
        """ Creates an empty population set

        """

        self.set = []

        return None
    # end empty_population

    
    def random_population(self):
        """ Creates a population with random individuals

        """

        pass
    # end random_population


    def expert_population(self):
        """ Creates a population with external knowledge

        """

        pass
    # end expert_population


    def fromfile_population(self, file_name):
        """ Creates a population from file

        """

        with open(file_name, 'r') as fd:
            for line in fd:
                condition, action = line.split(' ',1)
                cl = Classifier()
                cl.from_str(condition, action)
                self.insert(cl)

        pass
    # end fromfile_population


    def insert(self, cl):
        """ insert classifier in the population

            Inserts classifier cl in the population set. If exists a classifier
            with the same action and condition, it increments its numerosity

        """

        # if there's another classifier with the same action and condition it
        # only increases its numerosity
        for c in self.set:
            if c.equals(cl):
                c.incr_numerosity(1)
                return


        # otherwise inserts the new classifier in the set
        self.set.append(cl)

        return None
    # end insert


    def delete(self, check_sizeconstraint=True):
        """ delete classifier from the population

            Choose a classifier from the population using a selection method
            and remove - decrease its numerosity - from the population.
        """

        if check_sizeconstraint:
            if self.micro_sum() < cons.N:
                return
        
        avfit = self.get_avg_fitness()
        for cl in self.set:
            cl.set_avfit(avfit)

        self.selection.init(self)

        del_cl = self.selection.select()

        for cl in self.set:
            if cl.equals(del_cl):
                if cl.get_numerosity() > 1:
                    cl.incr_numerosity(-1)
                else:
                    self.set.remove(cl)
                return del_cl
                


        pass
    # end delete


    def form_match_set(self, tstamp, curr_inst):
        """ Forms a match set

            Forms a match set with the classifiers that match the curr_inst
            condition

            Returns the match set with the classifiers or empty if none are
                found to match the curr_inst condition
        """

        m_set = MatchSet(self)

        while m_set.isempty():
            for cl in self.set:
                if cl.match(curr_inst):
                    m_set.set.append(cl)

            m_set_classes = len(m_set.get_actions())
            if m_set_classes < cons.theta_mna:
                m_set.covering(tstamp, curr_inst)
                m_set.empty()


        return m_set
    # end form_match_set


    def form_eval_set(self, curr_inst):
        """ Forms a match set por evaluation purposes

            Forms a match set with the classifiers that match the curr_inst
            condition

            Returns the match set with the classifiers or empty if none are
                found to match the curr_inst condition
        """

        m_set = MatchSet(self)

        for cl in self.set:
            if cl.match(curr_inst):
                m_set.set.append(cl)

        return m_set
    # end form_eval_set


    def eval(self, is_test=True):
        """ Evaluate the population

            - is_test is True if it is to be evaluated under the test cases;
                False if it is with the training cases

        """

        y_true = []
        y_score = []

        uncovered = 0
        
        if is_test:
            num_tests = cons.env.n_testing_instances
        else:
            num_tests = cons.env.n_training_instances

        for x in range(num_tests):

            if is_test:
                inst = cons.env.get_next_testing_instance()
            else:
                inst = cons.env.get_next_test_training_instance()

            m_set = self.form_eval_set(inst)

            if m_set.isempty():
                uncovered += 1

            #pred_array = PredWeightedPrediction(m_set)
            pred_array = BTSPrediction(m_set)

            b_action = pred_array.best_action()

            y_true.append(inst[-1])
            y_score.append(b_action)

        return (y_true, y_score, uncovered)
    # end eval
