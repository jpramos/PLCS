
"""
Name:        LCS.py
Authors:     Joao P. Ramos - Written at University of Coimbra, Coimbra,
                Portugal
Contact:     jpramos@dei.uc.pt
Created:     February 2017
Description: This class controls the LCS run.

"""

# Import required modules ---------------------
from Constants import *
from Population import Population
from MatchSet import MatchSet
from CorrectSet import CorrectSet
from GA import GA
from Statistics import Statistics
from AccWeightedPrediction import AccWeightedPrediction
from PredWeightedPrediction import PredWeightedPrediction
from BTSPrediction import BTSPrediction

import random as rand
import logging
# ---------------------------------------------


# Get Logger info
LOGGER = logging.getLogger(__name__)

class LCS:

    def __init__(self, env=None, rp=None):
        """ Class Constructor

        """

        # save environment reference
        self.env = env

        # save reinforcement program reference
        self.rp = rp

        # init time-step
        self.time_t = 1

        # init population
        # either start
        # (1) an empty population
        # (2) at random
        # (3) from previous set of knowledge rules
        # (4) from file
        init_pop = 4
        self.population = Population(init_pop, 'multiplexer_11_perfect.pop')

        self.prev_correct_set = CorrectSet()
        self.prev_rho = 0
        self.prev_inst = None

        pass
    # end Class Constructor


    def run(self):
        """ run the system

        """

        # run evolution
        self.start_evol()

        # clean-up and statistics
        self.finish()

        pass
    # end run


    def start_evol(self):
        """ run evolution

        """

        self.prev_rho = 0
        ga = GA()


        ## TEST BTS
        count_bts = 0 # count number of times bts is right
        count_pred = 0 # count number of times prediction is right

        while(self.time_t < cons.max_iter):

            # read an instance
            instance = self.env.get_next_training_instance()

            
            # test - 
            print(instance)

            # form a match set
            match_set = self.population.form_match_set(self.time_t, instance)

            for c in match_set.set:
                print(c.print_condition())

            # form a prediction array
            #pred_array = PredWeightedPrediction(match_set)
            bts_array = PredWeightedPrediction(match_set)
            pred_array = BTSPrediction(match_set)

            # select action according to the prediction array
            if (rand.random() < cons.p_explr):
                action = pred_array.explore()
                bts_action = bts_array.explore()
            else:
                action = pred_array.best_action()
                bts_action = bts_array.best_action()

            if bts_action == instance[-1]:
                count_bts += 1

            if action == instance[-1]:
                count_pred += 1

            # form an correct set
            correct_set = match_set.form_correct_set(action)

            # execute action, get_reward
            rho = self.rp.get_reward(action, pred_array)


            if not self.prev_correct_set.isempty():
                P = self.prev_rho + cons.gamma * max(pred_array)
                # update set prev_correct_set using P
                self.prev_correct_set.update_set(P, instance[-1])
                self.prev_correct_set.specify(self.population, instance, self.time_t)
                ga.run_ga(self.time_t, self.prev_correct_set, instance,
                        self.population)

            if self.rp.is_end_of_problem():
                P = rho
                # update correct set A using P
                correct_set.update_set(P, instance[-1])
                correct_set.specify(self.population, instance, self.time_t)
                # run Ga in correct set A
                ga.run_ga(self.time_t, correct_set, instance, self.population)
                self.prev_correct_set.empty()
            else:
                self.prev_correct_set = correct_set
                self.prev_rho = rho
                self.prev_inst = instance
            
            match_set.update_set(instance[-1])
            #print('correct_size: {}\t{}'.format(correct_set.size(),
            #    correct_set.micro_sum()))

            #ga.run_ga(self.time_t, correct_set, instance, self.population)


            if self.time_t % 100 == 0:
                stats = self.eval(False)
                self.write_results_to_file(stats, 'tmpeval.txt')

                print('pred_times: {} / bts_times:{}'.format(count_pred/float(self.time_t),
                        count_bts/float(self.time_t)))

            self.write_data_in_file(self.population.get_avg_fitness(),
                    'avgfit.txt')
            self.write_data_in_file(max([c.get_mfitness() for c in
                self.population.set]), 'maxfit.txt')
            self.write_data_in_file(min([c.get_mfitness() for c in
                self.population.set]), 'minfit.txt')

            self.write_data_in_file(self.population.micro_sum(), 'nclass.txt')
            self.write_data_in_file(sum([c.get_exp() for c in
                self.population.set])/self.population.size(),
                'avgexp.txt')

            #if self.time_t in range(2850, 2950):
            #    self.eval(False)

            for c in match_set.set:
                c.print_atts()

            
            # increase time
            self.time_t += 1

        pass
    # end start_evol


    def finish(self):
        """ finish this run

        """

        stats = self.eval()

        self.population.write_conditions_to_file('pop.txt')

        file_name = 'eval.txt'
        self.write_results_to_file(stats, file_name)

        pass
    # end finish


    def eval(self, is_test=True):
        """ eval current population

        """

        (y_true, y_score, uncovered) = self.population.eval(is_test)

        stats = Statistics(y_true, y_score)

        print('sp: {}, se: {}, g_m: {}\tuncovered:{}'.format(stats.specificity(),
            stats.sensitivity(), stats.g_mean(), uncovered))

        return stats

    # end eval


    def write_results_to_file(self, stats, file_name):
        """ Write classification results to file

            write classification results, contained in stats, into file_name

        """

        with open(file_name, 'a') as fd:
            fd.write('{}\t{}\t{}\t{}\n'.format(stats.accuracy(),
                stats.sensitivity(), stats.specificity(), stats.g_mean()))

        return None
    # end write_results_to_file


    def write_data_in_file(self, data, data_file):
        """ Write data in a file

            write data data in a file data_file

        """

        with open(data_file, 'a') as fd:
            fd.write('{}\n'.format(data))

    # end write_data_in_file


