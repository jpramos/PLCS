
"""
Name:        GA.py
Authors:     Joao P. Ramos - Written at University of Coimbra, Coimbra,
                Portugal
Contact:     jpramos@dei.uc.pt
Created:     February 2017
Description: This class controls the genetic algorithm.

"""

# Import required modules ---------------------
from Constants import *
from Classifier import Classifier
from OnePointCrossover import OnePointCrossover
from Mutation import Mutation
from KMutation import KnowledgedMutation
from TournamentSelection import TournamentSelection
from KTournamentSelection import KnowledgeTournamentSelection
from RouletteSelection import RouletteSelection
from KRouletteSelection import KnowledgeRouletteSelection

import random as rand
import logging
# ---------------------------------------------


# Get Logger info
LOGGER = logging.getLogger(__name__)

class GA:

    def __init__(self):
        """ Class constructor

        """

        # init selection operator
        if cons.ga_sel == 'RWS':
            self.selection = RouletteSelection('fitness')
        elif cons.ga_sel == 'KRWS':
            self.selection = KnowledgeRouletteSelection('fitness')
        elif cons.ga_sel == 'TS':
            self.selection = TournamentSelection(cons.ga_tour_size, 'fitness')
        elif cons.ga_sel == 'KTS':
            self.selection = KnowledgeTournamentSelection(cons.ga_tour_size,
                    'fitness')


        # init crossover operator
        if cons.crossover == '1PT':
            self.crossover = OnePointCrossover()


        if cons.mutation == 'SM':
            self.mutation = Mutation()
        else:
            self.mutation = KnowledgedMutation()
        

        pass
    # end Class Constructor


    def run_ga(self, tstamp, set, inst, pop):
        """ Runs GA

            Runs GA on the set, usually the correct set

        """

        if ((tstamp - set.average_cl_time()) >= cons.theta_GA) and (set.size() > 0):

            LOGGER.debug('Entering GA...')

            set.reset_cl_time(tstamp)

            self.selection.init(set)

            parent_1 = self.selection.select()
            parent_2 = self.selection.select()

            child_1 = Classifier()
            child_1.clone(parent_1)

            child_2 = Classifier()
            child_2.clone(parent_2)

            if rand.random() < cons.chi:
                child_1.cl_param.xover_parameters(parent_1.cl_param,
                        parent_2.cl_param, tstamp)
                child_2.cl_param.xover_parameters(parent_2.cl_param,
                        parent_1.cl_param, tstamp)
                
                # crossover
                child_1, child_2 = self.crossover.crossover(child_1, child_2,
                        parent_1, parent_2)

            else:
                child_1.cl_param.clone_parameters(parent_1.cl_param, tstamp)
                child_2.cl_param.clone_parameters(parent_2.cl_param, tstamp)


            self.mutation.mutate(child_1, inst)
            self.mutation.mutate(child_2, inst)

            if cons.do_GA_subsumption:

                self.do_ga_subsumption((parent_1, parent_2), (child_1, child_2),
                    pop)
            else:

                pop.insert(child_1)
                pop.delete()

                pop.insert(child_2)
                pop.delete()



    # end run_ga


    def do_ga_subsumption(self, parents, children, pop):
        """ Do GA subsumption

        """

        for child in children:

            if parents[0].does_subsume(child):
                parents[0].incr_numerosity(1)
            elif parents[1].does_subsume(child):
                parents[1].incr_numerosity(1)
            else:
                pop.insert(child)

            pop.delete()

    # end do_ga_subsumption
