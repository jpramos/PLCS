#!/bin/bash

# file names
declare -a file_array=("avgexp.txt"
                            "avgfit.txt"
                            "eval.txt"
                            "maxfit.txt"
                            "minfit.txt"
                            "nclass.txt"
                            "pop.txt"
                            "tmpeval.txt")

# bck folder
declare -a bck_folder="bck_data"

# remove files
for file in "${file_array[@]}"; do
    yes | cp "$file" "$bck_folder"
    rm "$file"
done

