
"""
Name:        TournamentSelection.py
Authors:     Joao P. Ramos - Written at University of Coimbra, Coimbra,
                Portugal
Contact:     jpramos@dei.uc.pt
Created:     February 2017
Description: This class controls a tournament selection mechanism.

"""

# Import required modules ---------------------
from Constants import *
from Classifier import Classifier
from Selection import Selection

import random as rand
import logging
# ---------------------------------------------


# Get Logger info
LOGGER = logging.getLogger(__name__)

class TournamentSelection(Selection):

    def __init__(self, t_size, s_function):
        """ Class Constructor

            t_size - tournament size

            s_function - value to which the classifiers are sorted
                - fitness
                - deletion_vote

        """

        Selection.__init__(self, s_function)

        self.tour_size = t_size

        pass
    # end Class Constructor


    def init(self, set):
        """ Initialize selection operator with the current set

        """

        self.set = set
    # end init

    
    def select(self):
        """ selects an individual from the set

            Returns the individual
        """

        max_v = -1.0
        cl_t = None
        while cl_t is None:
            for cl in self.set.set:
                if self.call_s_fun(cl, self.s_fun) > max_v:
                    j = 0
                    while j < cl.get_numerosity():
                        if rand.random() < self.tour_size:
                            cl_t = cl
                            max_v = self.call_s_fun(cl, self.s_fun)
                            break
                        j+=1

        return cl_t
    # end select
