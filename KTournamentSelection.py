
"""
Name:        KTournamentSelection.py
Authors:     Joao P. Ramos - Written at University of Coimbra, Coimbra,
                Portugal
Contact:     jpramos@dei.uc.pt
Created:     February 2017
Description: This class controls a tournament selection mechanism, with a
                knowledge bias.

"""

# Import required modules ---------------------
from Constants import *
from Classifier import Classifier
from TournamentSelection import TournamentSelection

import random as rand
import logging
# ---------------------------------------------


# Get Logger info
LOGGER = logging.getLogger(__name__)

class KnowledgeTournamentSelection(TournamentSelection):

    def __init__(self, t_size, s_function):
        """ Class Constructor

            t_size - tournament size

            s_function - value to which the classifiers are sorted
                - fitness
                - deletion_vote

        """

        TournamentSelection.__init__(self, t_size, s_function)

        pass
    # end Class Constructor


    def select(self):
        """ selects an individual from the set

            Returns the individual
        """

        max_v = -1.0
        cl_t = None
        while cl_t is None:
            for cl in self.set.set:
                bias = cons.env.ruleData.match_rule_classifier(cl)
                curr_prob = self.call_s_fun(cl, self.s_fun)
                curr_prob = curr_prob * (1 + bias)
                #curr_prob += bias
                if curr_prob > max_v:
                    j = 0
                    while j < cl.get_numerosity():
                        if rand.random() < self.tour_size:
                            cl_t = cl
                            max_v = curr_prob
                            break
                        j+=1

        return cl_t
    # end select
