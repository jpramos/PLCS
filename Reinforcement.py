
"""
Name:        Reinforcement.py
Authors:     Joao P. Ramos - Written at University of Coimbra, Coimbra,
                Portugal
Contact:     jpramos@dei.uc.pt
Created:     February 2017
Description: This class controls the Reinforcement Learning module.

"""

# Import required modules ---------------------
from Constants import *

import logging
# ---------------------------------------------


# Get Logger info
LOGGER = logging.getLogger(__name__)

class Reinforcement:

    def __init__(self, env=None):
        """ Class Constructor

        """

        self.env = env

    # end Class Constructor


    def get_reward(self, action, pa):
        """ Calculate reward

        """

        curr_instance = self.env.get_current_training_instance()

        if curr_instance[-1] == float(action):
            reward = 1000
        else:
            reward = 0

        return reward
    # end get_reward


    def is_end_of_problem(self):
        """ 

        """

        return True
    # end is_end_of_problem

# end Class

