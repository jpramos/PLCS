

"""
Name:        Attribute.py
Authors:     Joao P. Ramos - Written at University of Coimbra, Coimbra,
                Portugal
Contact:     jpramos@dei.uc.pt
Created:     February 2017
Description: This class defines the attribute's main representation. Specific
                representations inherit from this class.

"""

# Import required modules --
from Constants import *

import logging
# --------------------------

# Get Logger info.
LOGGER = logging.getLogger(__name__)


class Attribute:

    def __init__(self, ai=None):
        """ Class Constructor

        """

        self.allele_index = ai

        pass
    # end Class Constructor


    def mutate(self):
        """ Mutates the allele
        
        """

        pass
    # end mutate
