
"""
Name:        Set.py
Authors:     Joao P. Ramos - Written at University of Coimbra, Coimbra,
                Portugal
Contact:     jpramos@dei.uc.pt
Created:     February 2017
Description: This is the main class for all sets in the framework.

"""

# Import required modules ---------------------
from Constants import *

import logging
# ---------------------------------------------


# Get Logger info
LOGGER = logging.getLogger(__name__)

class Set:

    def __init__(self):
        """ Class Constructor

        """

        #init set
        self.set = []

        pass
    # end Class Constructor


    def empty(self):
        """ empties set

        """

        self.set = []

        return None
    # end empty


    def isempty(self):
        """ check if set is empty

            Returns True if is empty; False otherwise
        """

        return len(self.set) == 0
    # end isempty


    def has_classifier(self, cl):
        """ check if set has a classifier

            Check if set has the cl classifier

            Returns the classifier position if True; None otherwise
        """

        return self.set.index(cl) if cl in self.set else None
    # end has_classifier


    def size(self):
        """ get set's macro size

            Returns set's size
        """

        return len(self.set)
    # end set_size


    def micro_sum(self):
        """ get set's micro size

            Returns set's micro size
        """

        return sum([cl.get_numerosity() for cl in self.set])
    # end micro_sum


    def get_avg_acc(self):
        """ get accuracy average in the set

            Returns -1 if set is empty; accuracy average otherwise
        """

        if self.isempty():
            return -1

        whole_acc = sum([cl.get_acc() for cl in self.set])
        
        return whole_acc / float(self.size())
    # end get_avg_acc


    def get_avg_prediction(self):
        """ get prediction average in the set

            Returns -1 if set is empty; prediction average otherwise
        """

        if self.isempty():
            return -1

        whole_pred = sum([cl.get_prediction() for cl in self.set])

        return whole_pred / float(self.size())
    #end get_avg_pred


    def get_avg_fitness(self):
        """ get fitness average in the set

            Returns -1 if set is empty; fitness average otherwise.
        """

        if self.isempty():
            return -1

        whole_fitness = sum([cl.get_Mfitness() for cl in self.set])

        return whole_fitness / self.micro_sum()
    # end get_avg_fitness


    def reset_cl_time(self, ts):
        """ resets classifiers time

            resets classifiers time with current time ts
        """

        for cl in self.set:
            cl.set_time(ts)

        return None
    # end reset_cl_time


    def average_cl_time(self):
        """ calculates average time of classifiers in the set

            Returns the average time
        """

        avg_time = sum([cl.get_time() * cl.get_numerosity() * 1.0 for cl in
            self.set])

        return float(avg_time) / float(self.micro_sum())
    # end average_cl_time


    def is_experienced(self):
        """ Find average experience in the set

            Calculates the average experience within the classifiers present
            in the set.

            Returns True if average experience is higher than a specific
            threshold; False otherwise.
        """

        whole_exp = sum([cl.get_exp() * cl.get_numerosity() for cl in self.set])

        avg_exp = float(whole_exp) / float(self.micro_sum())

        if avg_exp > cons.N_sp:
            return True
        else:
            return False

    # end is_experienced


    def print_set(self):
        """ Print set to log file

        """

        if self.isempty():
            return None

        LOGGER.debug(self.set[0].print_cl(True))
        for cl in self.set[1:]:
            LOGGER.debug(cl.print_cl(False))


        return None
    # end print_set


    def write_set_to_file(self, file_name):
        """ write set to file

            Writes set to file with name file_name

        """

        if self.isempty():
            return None

    
        with open(file_name, 'wb') as fp:
            
            fp.write(self.set[0].print_cl() + '\n')
            for cl in self.set[1:]:
                fp.write(cl.print_cl() + '\n')


        return None
    # end write_set_to_file
        

    def write_conditions_to_file(self, file_name):
        """ write conditions to file
        
            Write classifier conditions to file with name file_name
        """

        if self.isempty():
            return None

        with open(file_name, 'wb') as fd:

            for cl in self.set:
                #fd.write(' '.join(map(str, cl.condition)))
                fd.write(cl.print_condition())
                fd.write('\t{}\t{}\t{}\t{}'.format(cl.action, cl.cl_param.exp,
                    cl.cl_param.prediction, cl.others_prediction))
                fd.write('\n')


        return None
    # end write_conditions_to_file

