
"""
Name:        BTSPrediction.py
Authors:     Joao P. Ramos - Written at University of Coimbra, Coimbra,
                Portugal
Contact:     jpramos@dei.uc.pt
Created:     February 2017
Description: This class builds a Bayesian Truth Serum (BTS) weighted prediction array to select the best action.

"""

# Import required modules ---------------------
from Constants import *
from PredictionArray import PredictionArray
from MatchSet import MatchSet

import logging
# ---------------------------------------------


# Get Logger info
LOGGER = logging.getLogger(__name__)

class BTSPrediction(PredictionArray):


    def __init__(self, m_set):
        """ Class Constructor

            m_set: match set from which the prediction array is built

        """

        PredictionArray.__init__(self)

        if m_set.size() == 0:
            return

        avg_endors = self.calc_avg_endorsements(m_set)
        gmean_preds = self.calc_gmean_predictions(m_set)
        n_respondents = float(m_set.size())

        for cl in m_set.set:
            ind = cons.env.all_classes.index(cl.action)
            self.pred_array[ind] += cl.get_bts_score(avg_endors, gmean_preds)
            self.num_sum[ind] += 1

        self.pred_array[:] = [abs(pred / (n_respondents * avg_e)) if avg_e > 0 else
                0 for (pred, avg_e) in zip (self.pred_array, avg_endors)]

    # end Class Constructor


    def calc_avg_endorsements(self, m_set):
        """ Calculate the endorsement average of each class 

        """

        count_endors = [0] * cons.env.num_classes
        n_respondents = float(m_set.size())

        for cl in m_set.set:
            ind = cons.env.all_classes.index(cl.action)
            count_endors[ind] += 1

        avg_endors = [c/n_respondents for c in count_endors]

        return avg_endors
    # end calc_avg_endorsements


    def calc_gmean_predictions(self, m_set):
        """ Calculate the prediction's gmean of each class

        """

        n_respondents = float(m_set.size())
        c_predictions = [1] * cons.env.num_classes
        
        for cind in range(cons.env.num_classes):
            for cl in m_set.set:
                pred = cl.get_others_prediction(cind)
                if pred > 0:
                    c_predictions[cind] *= pred
            c_predictions[cind] = (c_predictions[cind] **
                    (1/float(n_respondents)))


        return c_predictions
    # end calc_gmean_predictions


        
