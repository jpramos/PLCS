
"""
Name:        Rule.py
Authors:     Joao P. Ramos - Written at University of Coimbra, Coimbra,
                Portugal
Contact:     jpramos@dei.uc.pt
Created:     February 2017
Description: This class represents a IF-THEN rule.

"""

# Import required modules ---------------------
from Constants import *

import logging
import math
# ---------------------------------------------


# Get Logger info
LOGGER = logging.getLogger(__name__)

class Rule:

    def __init__(self, att_ref=None, att=None, op=None, th=None, action=None, conf_l=
            None):
        """ Class Constructor

            The rule's representation follows the template
            <attribute> <cond> <threshold> <then> <action>

            - att_ref is the attribute's reference saved in the environment
            - att is the attribute itself
            - op is the operator used in the rule (<, >, =)
            - th is the threshold defined in the rule
            - action is the statement that follows the truth of the rule's
              condition
            - conf_l is the confidence level of the expert on the given rule

        """


        self.att_ref = att_ref      # attributes's reference in the environment
        self.att = att              # attribute used in the rule

        self.action = action        # action given by the rule

        self.threshold = th         # threshold defined in the rule

        self.conf_level = conf_l    # confidence level of the expert on this
                                    # rule

        self.operator = op          # operator used in the rule

    # end Class Constructor

    
    def activate_rule(self, th, max_val, min_val):
        """ Activate rule

        """

        dist = abs(th - self.threshold)

        p_radius = .10
        sig = (float(max_val) - float(min_val)) * p_radius

        if (dist <= sig):
            bias = self.conf_level * math.e ** (-(th-self.threshold)**2 /
                    (2*sig**2))
        else:
            bias = 0

        return bias
    # end activate_rule
