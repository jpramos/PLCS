
"""
Name:        DiscrRep.py
Authors:     Joao P. Ramos - Written at University of Coimbra, Coimbra,
                Portugal
Contact:     jpramos@dei.uc.pt
Created:     February 2017
Description: This class defines a discrete representation for the attributes.

"""

# Import required modules --
from Constants import *
from Attribute import Attribute

import random as rand
import logging
# --------------------------

# Get Logger info
LOGGER = logging.getLogger(__name__)



class DiscreteRep(Attribute):

    def __init__(self, ai=None, state=None):
        """ Class Constructor

        """

        Attribute.__init__(self, ai)

        self.allele = state

        pass
    # end Class Constructor


    def mutate(self):
        """ mutate function

        """

        att_info = cons.env.attribute_info[self.allele_index]

        possible_genes = [g for g in att_info['att_info'] if float(g) !=
                self.allele]
        self.allele = float(rand.choice(possible_genes))

    # end mutate


    def equals(self, o_att):
        """ Compare this attribute with o_att

            Returns True if equals; False otherwise
        """

        if self.allele == o_att.allele:
            return True

        return False
    # end equals


    def match(self, inst_att):
        """ Check if this attribute matches with instance's attribute

            Returns True if it matches; False otherwise
        """

        if self.allele == inst_att or inst_att == cons.label_missing_data:
            return True
        else:
            return False

    # end match


    def is_more_general(self, o_att):
        """ Is more general function for discrete attributes

            Check if this attribute is more general than o_att

            Return True if it is more general; False otherwise
        """

        if self.allele != o_att.allele:
            return False

        return True
    # end is_more_general
