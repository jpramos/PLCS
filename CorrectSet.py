
"""
Name:        CorrectSet.py
Authors:     Joao P. Ramos - Written at University of Coimbra, Coimbra,
                Portugal
Contact:     jpramos@dei.uc.pt
Created:     February 2017
Description: This is the class that represents the correct set in LCS. In a
                correct set are the classifiers that match the condition of the
                current instance.

"""

# Import required modules ---------------------
from Constants import *
from Set import Set
from Classifier import Classifier
from KClassifier import KnowledgeClassifier
from RouletteSelection import RouletteSelection

import logging
# ---------------------------------------------


# Get Logger info
LOGGER = logging.getLogger(__name__)

class CorrectSet(Set):

    def __init__(self, m_set=None):
        """ Class Constructor

            - m_set is the parent match set from which this correct set is
              created

        """

        Set.__init__(self)

        self.m_set = m_set

        pass
    # end Class Constructor


    def covering(self, tstamp, curr_inst, curr_action):
        """ Covering mechanism

            Creates a new classifier that matches curr_inst and curr_action and
            adds it to all sets

        """

        # creates new classifier
        if cons.cl_cov == 'KCL':
            cl = KnowledgeClassifier()
        else:
            cl = Classifier()
        cl.covering(tstamp, curr_inst, curr_action)

        # TODO See if it's necessary a loop like in KEEL-UCS


        if self.m_set.parent_pop.micro_sum() + 1 >= cons.N:

            del_cl = self.m_set.parent_pop.delete(False)

            pos = self.m_set.has_classifier(del_cl)
            if pos is not None:
                self.m_set.set.remove(del_cl)

            pos = self.has_classifier(del_cl)
            if pos is not None:
                self.set.remove(del_cl)



        # add the new classifier to all sets
        self.set.append(cl)
        self.m_set.set.append(cl)
        self.m_set.parent_pop.insert(cl)
        #self.m_set.parent_pop.delete()

    # end covering


    def specify(self, pop, inst, curr_time):
        """ Specify operator in the correct set

            If a current correct set is sufficiently experienced and its
            average error exceeds a pre-defined threshold, then it randomly
            selects a rule, probability proportional to the classifier's
            accuracy, and specifies some of its do not care symbols, according
            to the current instance
        """

        #if self.get_avg_acc() <= (pop.get_avg_acc() / 2.0) and self.is_experienced():
        if (self.get_avg_prediction() >= (pop.get_avg_prediction() * 2.0) and
                self.is_experienced()):

            s_num = self.micro_sum()

            #select = RouletteSelection('accuracy')
            select = RouletteSelection('prediction')

            select.init(self)
            
            sel_cl = select.select()

            new_cl = Classifier()
            new_cl.clone(sel_cl)
            new_cl.set_time(curr_time)
            
            new_cl.specify(inst)

            pop.insert(new_cl)
            pop.delete()

            return
    # end specify


    def update_set(self, P, inst_action):
        """ Update classifiers parameters

            Update classifiers parameters after correct set matching

        """

        for cl in self.set:
            cl.update_parameters(P, self.micro_sum())
            #cl.update_seen(inst_action)
            #cl.update_others_prediction(self.m_set)

        
        k_sum = 0
        k_vec = [0] * len(self.set)

        for (ind, cl) in enumerate(self.set):
            if cl.get_eps() < cons.eps_0:
                k_vec[ind] = 1
            else:
                k_vec[ind] = cons.alpha * (cl.get_eps() / float(cons.eps_0)) ** (-1.0
                    * cons.nu)

            k_sum += k_vec[ind] * cl.get_numerosity()


        for (i, cl) in enumerate(self.set):
            cl.update_fitness(k_sum, k_vec[i])

    # end update_set

