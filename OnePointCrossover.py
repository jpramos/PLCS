
"""
Name:        OnePointCrossover.py
Authors:     Joao P. Ramos - Written at University of Coimbra, Coimbra,
                Portugal
Contact:     jpramos@dei.uc.pt
Created:     February 2017
Description: This class controls the crossover mechanism with one cut out
                point.

"""

# Import required modules ---------------------
from Constants import *

import random as rand
import copy as cp
import logging
# ---------------------------------------------


# Get Logger info
LOGGER = logging.getLogger(__name__)

class OnePointCrossover:

    def __init__(self):
        """ Class Constructor

        """

        pass
    # end Class Constructor

    
    def crossover(self, child_1, child_2, parent_1, parent_2):
        """ Apply the crossover mechanism

            Returns 2 offsprings child_1, child_2
        """

        cut_point = rand.randint(1, cons.env.num_attributes-1)

        p1_ind = -1
        for (p_ind, r_ind) in enumerate(parent_1.spec_index):
            if r_ind >= cut_point:
                p1_ind = p_ind
                break
        if p1_ind == -1:
            p1_ind = len(parent_1.spec_index)

        p2_ind = -1
        for (p_ind, r_ind) in enumerate(parent_2.spec_index):
            if r_ind >= cut_point:
                p2_ind = p_ind
                break
        if p2_ind == -1:
            p2_ind = len(parent_2.spec_index)


        p1_c = cp.deepcopy(parent_1.condition)
        p2_c = cp.deepcopy(parent_2.condition)

        child_1.condition = p1_c[:p1_ind] + p2_c[p2_ind:]
        child_1.spec_index = parent_1.spec_index[:p1_ind] + parent_2.spec_index[p2_ind:]

        child_2.condition = p2_c[:p2_ind] + p1_c[p1_ind:]
        child_2.spec_index = parent_2.spec_index[:p2_ind] + parent_1.spec_index[p1_ind:]

        #child_1.condition = parent_1.condition[:p1_ind] + parent_2.condition[p2_ind:]
        #child_1.spec_index = parent_1.spec_index[:p1_ind] + parent_2.spec_index[p2_ind:]

        #child_2.condition = parent_2.condition[:p2_ind] + parent_1.condition[p1_ind:]
        #child_2.spec_index = parent_2.spec_index[:p2_ind] + parent_1.spec_index[p1_ind:]


        return child_1, child_2
    # end crossover
