
"""
Name:        Mutation.py
Authors:     Joao P. Ramos - Written at University of Coimbra, Coimbra,
                Portugal
Contact:     jpramos@dei.uc.pt
Created:     February 2017
Description: This class controls the mutation mechanism.

"""

# Import required modules ---------------------
from Constants import *
from RealRep import RealRep
from DiscrRep import DiscreteRep

import random as rand
import logging
# ---------------------------------------------


# Get Logger info
LOGGER = logging.getLogger(__name__)

class Mutation:

    def __init_(self):
        """ Class Constructor

        """

        pass
    # end Class Constructor


    def mutate(self, cl, curr_inst):
        """ mutates cl

            Mutates individual cl within the domain of current instance

            Returns mutated individual
        """


        for ind in xrange(cons.env.num_attributes):
            att_info = cons.env.attribute_info[ind]

            if rand.random() < cons.mu:

                if ind not in cl.spec_index:
                    att_ind = -1
                    for i in cl.spec_index:
                        if i >= ind:
                            att_ind = cl.spec_index.index(i)
                            break
                    if att_ind == -1:
                        att_ind = len(cl.spec_index)

                    if att_info['is_continuous']:
                        tmp = RealRep(ind)
                        tmp.build_interval(curr_inst[ind])
                        cl.condition.insert(att_ind, tmp)
                    else:
                        cl.condition.insert(att_ind, DiscreteRep(ind,
                            curr_inst[ind]))
                    cl.spec_index.insert(att_ind, ind)

                else:

                    att_ind = cl.spec_index.index(ind)
                    
                    if rand.random() < .5:

                        cl.condition.pop(att_ind)
                        cl.spec_index.pop(att_ind)

                    else:

                        cl.condition[att_ind].mutate()

        
        if rand.random() < cons.mu:
            other_options = [x for x in cons.env.all_classes if x != cl.action]
            cl.action = rand.choice(other_options)



    # end mutate
