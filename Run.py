

"""
Name:        Run.py
Authors:     Joao P. Ramos - Written at University of Coimbra, Coimbra,
                Portugal
Contact:     jpramos@dei.uc.pt
Created:     February 2017
Description: Init LCS.

"""

# Import required modules --

from Constants import *
from LCS import LCS
from OfflineEnvironment import OfflineEnvironment
from Reinforcement import Reinforcement
import random as rand

import logging
from logger_defs import logger_defs

logger_defs.setup_logging()
LOGGER = logging.getLogger(__name__)

CONFIG_FILE = 'PLCS_CONFIG.txt'

def read_config_file(config_file):

    comment_char = '%'
    param_char = '='

    parameters = {}

    with open(config_file, 'r') as f:
        for rline in f:

            # remove comments in line
            if comment_char in rline:
                rline, comment = rline.split(comment_char, 1)

            # get parameters and their values
            if param_char in rline:
                param, value = rline.split(param_char, 1)
                param = param.strip()
                value = value.strip()
                parameters[param] = value

    cons.set_constants(parameters)


def main():

    read_config_file(CONFIG_FILE)

    rand.seed(42)

    # init environment env
    env = OfflineEnvironment()
    cons.env = env

    # init reinforcement program rp
    rp = Reinforcement(env)

    # init LCS
    lcs = LCS(env, rp)

    # run
    lcs.run()


if __name__ == '__main__':
    main()
