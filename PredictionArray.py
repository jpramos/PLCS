
"""
Name:        PredictionArray.py
Authors:     Joao P. Ramos - Written at University of Coimbra, Coimbra,
                Portugal
Contact:     jpramos@dei.uc.pt
Created:     February 2017
Description: This class builds a prediction array to select the best action.

"""

# Import required modules ---------------------
from Constants import *
from MatchSet import MatchSet

import random as rand
import logging
# ---------------------------------------------


# Get Logger info
LOGGER = logging.getLogger(__name__)

class PredictionArray:

    def __init__(self):
        """ Class Constructor


        """

        self.pred_array = [0] * cons.env.num_classes
        self.num_sum = [0] * cons.env.num_classes

    # end Class Constructor


    def best_action(self):
        """ Gets best action

            Returns best action. If there are more than one it selects one
            randomly
        """
        

        comb = zip(range(len(self.pred_array)), self.pred_array)
        comb.sort(key=lambda x: x[1], reverse=True)

        return comb[0][0]
    # end best_action
        

    def explore(self):
        """ Randomly selects an action
        
            Returns a random action with a prediction > 0
        """

        not_null_ind = [ind for (ind, val) in enumerate(self.num_sum) if val >
                0]

        action = cons.env.all_classes[rand.choice(not_null_ind)]

        return action
    # end explore
